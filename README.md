# Exploiting Repetitions for Image-Based Rendering of Facades - Binaries and data

This repository is an auxiliary repository for https://gitlab.inria.fr/sibr/projects/facades-repetitions/facade_repetitions, 
which is an implementation of Exploiting Repetitions for Image-Based Rendering of Facades, 
Simon Rodriguez, Adrien Bousseau, Frédo Durand, George Drettakis, Computer Graphics Forum, Volume 37, Number 4 - 2018. (http://www-sop.inria.fr/reves/Basilic/2018/RBDD18/).  
Please refer to the documentation of the main repository for the detailed instructions.

It contains: 
- the required external tool binaries.
- Pix-2-Pix classifications weights: unzip `pix2pix-weights/model-data.7z` and rename to `model-400000.data-00000-of-00001`.
- KIPPI distribution: unzip `kippi/kippi-redist.7z` archive.
- An example dataset: unzip `datasets/ucl1.7z`.

## Required softwares

- Python 3.
- ImageMagick.
- Tensorflow (1.0.1) installed. I used a self-contained Anaconda environment, but had to use it from the Window command-line, and not Cygwin.
- SIBR compiled with the `BUILD_IBR_FACADE_REPETITIONS` flag enabled in CMake.
- OpenMVG modified version compiled and installed (available in this repo).
