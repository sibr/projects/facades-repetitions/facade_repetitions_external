# Install script for directory: D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/openMVG")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/HybridNonLinearSolver.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/LevenbergMarquardt.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/chkder.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/covar.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/dogleg.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/fdjac1.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/lmpar.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/qrsolv.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/r1mpyq.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/r1updt.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/rwupdt.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization" TYPE FILE FILES
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/HybridNonLinearSolver.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/LevenbergMarquardt.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/chkder.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/covar.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/dogleg.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/fdjac1.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/lmpar.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/qrsolv.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/r1mpyq.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/r1updt.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/src/NonLinearOptimization/rwupdt.h"
    )
endif()

