# Install script for directory: D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/openMVG")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/AdolcForward;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/AlignedVector3;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/ArpackSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/AutoDiff;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/BVH;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/FFT;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/IterativeSolvers;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/KroneckerProduct;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/LevenbergMarquardt;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/MatrixFunctions;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/MoreVectorization;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/MPRealSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/NonLinearOptimization;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/NumericalDiff;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/OpenGLSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/Polynomials;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/Skyline;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/SparseExtra;C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen/Splines")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files/openMVG/include/openMVG/third_party/eigen/unsupported/Eigen" TYPE FILE FILES
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/AdolcForward"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/AlignedVector3"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/ArpackSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/AutoDiff"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/BVH"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/FFT"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/IterativeSolvers"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/KroneckerProduct"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/LevenbergMarquardt"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/MatrixFunctions"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/MoreVectorization"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/MPRealSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/NonLinearOptimization"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/NumericalDiff"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/OpenGLSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/Polynomials"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/Skyline"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/SparseExtra"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/unsupported/Eigen/Splines"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("D:/sirodrig/code/openMVG/out/third_party/eigen/unsupported/Eigen/src/cmake_install.cmake")

endif()

