# Install script for directory: D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/openMVG")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/AmbiVector.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/CompressedStorage.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/MappedSparseMatrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseBlock.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseColEtree.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseCwiseBinaryOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseCwiseUnaryOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseDenseProduct.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseDiagonalProduct.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseDot.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseFuzzy.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseMatrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseMatrixBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparsePermutation.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseProduct.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseRedux.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseSelfAdjointView.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseSparseProductWithPruning.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseTranspose.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseTriangularView.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseUtil.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseVector.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/SparseView.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore/TriangularSolver.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/SparseCore" TYPE FILE FILES
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/AmbiVector.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/CompressedStorage.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/ConservativeSparseSparseProduct.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/MappedSparseMatrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseBlock.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseColEtree.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseCwiseBinaryOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseCwiseUnaryOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseDenseProduct.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseDiagonalProduct.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseDot.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseFuzzy.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseMatrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseMatrixBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparsePermutation.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseProduct.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseRedux.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseSelfAdjointView.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseSparseProductWithPruning.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseTranspose.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseTriangularView.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseUtil.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseVector.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/SparseView.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/SparseCore/TriangularSolver.h"
    )
endif()

