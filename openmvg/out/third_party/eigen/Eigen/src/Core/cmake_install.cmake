# Install script for directory: D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/openMVG")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Array.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/ArrayBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/ArrayWrapper.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Assign.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Assign_MKL.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/BandMatrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Block.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/BooleanRedux.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/CommaInitializer.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/CoreIterators.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/CwiseBinaryOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/CwiseNullaryOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/CwiseUnaryOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/CwiseUnaryView.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/DenseBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/DenseCoeffsBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/DenseStorage.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Diagonal.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/DiagonalMatrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/DiagonalProduct.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Dot.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/EigenBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Flagged.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/ForceAlignedAccess.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Functors.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Fuzzy.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/GeneralProduct.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/GenericPacketMath.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/GlobalFunctions.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/IO.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Map.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/MapBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/MathFunctions.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Matrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/MatrixBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/NestByValue.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/NoAlias.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/NumTraits.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/PermutationMatrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/PlainObjectBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/ProductBase.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Random.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Redux.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Ref.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Replicate.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/ReturnByValue.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Reverse.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Select.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/SelfAdjointView.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/SelfCwiseBinaryOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/SolveTriangular.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/StableNorm.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Stride.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Swap.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Transpose.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Transpositions.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/TriangularMatrix.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/VectorBlock.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/VectorwiseOp.h;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core/Visitor.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/src/Core" TYPE FILE FILES
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Array.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/ArrayBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/ArrayWrapper.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Assign.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Assign_MKL.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/BandMatrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Block.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/BooleanRedux.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/CommaInitializer.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/CoreIterators.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/CwiseBinaryOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/CwiseNullaryOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/CwiseUnaryOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/CwiseUnaryView.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/DenseBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/DenseCoeffsBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/DenseStorage.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Diagonal.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/DiagonalMatrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/DiagonalProduct.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Dot.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/EigenBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Flagged.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/ForceAlignedAccess.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Functors.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Fuzzy.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/GeneralProduct.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/GenericPacketMath.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/GlobalFunctions.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/IO.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Map.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/MapBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/MathFunctions.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Matrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/MatrixBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/NestByValue.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/NoAlias.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/NumTraits.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/PermutationMatrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/PlainObjectBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/ProductBase.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Random.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Redux.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Ref.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Replicate.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/ReturnByValue.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Reverse.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Select.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/SelfAdjointView.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/SelfCwiseBinaryOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/SolveTriangular.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/StableNorm.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Stride.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Swap.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Transpose.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Transpositions.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/TriangularMatrix.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/VectorBlock.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/VectorwiseOp.h"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/src/Core/Visitor.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("D:/sirodrig/code/openMVG/out/third_party/eigen/Eigen/src/Core/products/cmake_install.cmake")
  include("D:/sirodrig/code/openMVG/out/third_party/eigen/Eigen/src/Core/util/cmake_install.cmake")
  include("D:/sirodrig/code/openMVG/out/third_party/eigen/Eigen/src/Core/arch/cmake_install.cmake")

endif()

