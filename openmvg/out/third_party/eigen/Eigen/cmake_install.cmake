# Install script for directory: D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files/openMVG")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Devel" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Array;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Cholesky;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/CholmodSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Core;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Dense;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Eigen;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Eigen2Support;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Eigenvalues;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Geometry;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Householder;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/IterativeLinearSolvers;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Jacobi;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/LU;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/LeastSquares;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/MetisSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/OrderingMethods;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/PaStiXSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/PardisoSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/QR;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/QtAlignedMalloc;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SPQRSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SVD;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/Sparse;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SparseCholesky;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SparseCore;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SparseLU;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SparseQR;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/StdDeque;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/StdList;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/StdVector;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/SuperLUSupport;C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen/UmfPackSupport")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "C:/Program Files/openMVG/include/openMVG/third_party/eigen/Eigen" TYPE FILE FILES
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Array"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Cholesky"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/CholmodSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Core"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Dense"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Eigen"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Eigen2Support"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Eigenvalues"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Geometry"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Householder"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/IterativeLinearSolvers"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Jacobi"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/LU"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/LeastSquares"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/MetisSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/OrderingMethods"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/PaStiXSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/PardisoSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/QR"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/QtAlignedMalloc"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SPQRSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SVD"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/Sparse"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SparseCholesky"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SparseCore"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SparseLU"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SparseQR"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/StdDeque"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/StdList"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/StdVector"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/SuperLUSupport"
    "D:/sirodrig/code/openMVG/src/third_party/eigen/Eigen/UmfPackSupport"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("D:/sirodrig/code/openMVG/out/third_party/eigen/Eigen/src/cmake_install.cmake")

endif()

