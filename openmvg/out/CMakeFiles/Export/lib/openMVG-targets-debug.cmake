#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "lib_CoinUtils" for configuration "Debug"
set_property(TARGET lib_CoinUtils APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(lib_CoinUtils PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/lib_CoinUtils.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS lib_CoinUtils )
list(APPEND _IMPORT_CHECK_FILES_FOR_lib_CoinUtils "${_IMPORT_PREFIX}/lib/lib_CoinUtils.lib" )

# Import target "lib_Osi" for configuration "Debug"
set_property(TARGET lib_Osi APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(lib_Osi PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/lib_Osi.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS lib_Osi )
list(APPEND _IMPORT_CHECK_FILES_FOR_lib_Osi "${_IMPORT_PREFIX}/lib/lib_Osi.lib" )

# Import target "lib_clp" for configuration "Debug"
set_property(TARGET lib_clp APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(lib_clp PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/lib_clp.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS lib_clp )
list(APPEND _IMPORT_CHECK_FILES_FOR_lib_clp "${_IMPORT_PREFIX}/lib/lib_clp.lib" )

# Import target "lib_OsiClpSolver" for configuration "Debug"
set_property(TARGET lib_OsiClpSolver APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(lib_OsiClpSolver PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/lib_OsiClpSolver.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS lib_OsiClpSolver )
list(APPEND _IMPORT_CHECK_FILES_FOR_lib_OsiClpSolver "${_IMPORT_PREFIX}/lib/lib_OsiClpSolver.lib" )

# Import target "stlplus" for configuration "Debug"
set_property(TARGET stlplus APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(stlplus PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/stlplus.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS stlplus )
list(APPEND _IMPORT_CHECK_FILES_FOR_stlplus "${_IMPORT_PREFIX}/lib/stlplus.lib" )

# Import target "lemon" for configuration "Debug"
set_property(TARGET lemon APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(lemon PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/lemon.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS lemon )
list(APPEND _IMPORT_CHECK_FILES_FOR_lemon "${_IMPORT_PREFIX}/lib/lemon.lib" )

# Import target "cxsparse" for configuration "Debug"
set_property(TARGET cxsparse APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(cxsparse PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "C"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/cxsparse.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS cxsparse )
list(APPEND _IMPORT_CHECK_FILES_FOR_cxsparse "${_IMPORT_PREFIX}/lib/cxsparse.lib" )

# Import target "ceres" for configuration "Debug"
set_property(TARGET ceres APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(ceres PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "cxsparse"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/ceres-debug.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS ceres )
list(APPEND _IMPORT_CHECK_FILES_FOR_ceres "${_IMPORT_PREFIX}/lib/ceres-debug.lib" )

# Import target "easyexif" for configuration "Debug"
set_property(TARGET easyexif APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(easyexif PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/easyexif.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS easyexif )
list(APPEND _IMPORT_CHECK_FILES_FOR_easyexif "${_IMPORT_PREFIX}/lib/easyexif.lib" )

# Import target "fast" for configuration "Debug"
set_property(TARGET fast APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(fast PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/fast.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS fast )
list(APPEND _IMPORT_CHECK_FILES_FOR_fast "${_IMPORT_PREFIX}/lib/fast.lib" )

# Import target "openMVG_features" for configuration "Debug"
set_property(TARGET openMVG_features APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_features PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "fast"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_features.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_features )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_features "${_IMPORT_PREFIX}/lib/openMVG_features.lib" )

# Import target "openMVG_image" for configuration "Debug"
set_property(TARGET openMVG_image APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_image PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "openMVG_numeric;C:/Anaconda3/Library/lib/png.lib;C:/Anaconda3/Library/lib/z.lib;C:/Anaconda3/Library/lib/jpeg.lib;C:/Anaconda3/Library/lib/tiff.lib"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_image.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_image )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_image "${_IMPORT_PREFIX}/lib/openMVG_image.lib" )

# Import target "openMVG_lInftyComputerVision" for configuration "Debug"
set_property(TARGET openMVG_lInftyComputerVision APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_lInftyComputerVision PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "lib_clp;lib_OsiClpSolver;lib_CoinUtils;lib_Osi"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_lInftyComputerVision.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_lInftyComputerVision )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_lInftyComputerVision "${_IMPORT_PREFIX}/lib/openMVG_lInftyComputerVision.lib" )

# Import target "openMVG_matching" for configuration "Debug"
set_property(TARGET openMVG_matching APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_matching PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "openMVG_features;stlplus"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_matching.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_matching )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_matching "${_IMPORT_PREFIX}/lib/openMVG_matching.lib" )

# Import target "openMVG_kvld" for configuration "Debug"
set_property(TARGET openMVG_kvld APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_kvld PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_kvld.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_kvld )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_kvld "${_IMPORT_PREFIX}/lib/openMVG_kvld.lib" )

# Import target "openMVG_matching_image_collection" for configuration "Debug"
set_property(TARGET openMVG_matching_image_collection APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_matching_image_collection PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "openMVG_matching;openMVG_multiview"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_matching_image_collection.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_matching_image_collection )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_matching_image_collection "${_IMPORT_PREFIX}/lib/openMVG_matching_image_collection.lib" )

# Import target "openMVG_multiview" for configuration "Debug"
set_property(TARGET openMVG_multiview APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_multiview PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "openMVG_numeric;lemon;ceres;cxsparse"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_multiview.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_multiview )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_multiview "${_IMPORT_PREFIX}/lib/openMVG_multiview.lib" )

# Import target "openMVG_numeric" for configuration "Debug"
set_property(TARGET openMVG_numeric APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_numeric PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_numeric.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_numeric )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_numeric "${_IMPORT_PREFIX}/lib/openMVG_numeric.lib" )

# Import target "openMVG_system" for configuration "Debug"
set_property(TARGET openMVG_system APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_system PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_system.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_system )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_system "${_IMPORT_PREFIX}/lib/openMVG_system.lib" )

# Import target "openMVG_sfm" for configuration "Debug"
set_property(TARGET openMVG_sfm APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(openMVG_sfm PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "openMVG_multiview;stlplus;ceres;cxsparse;openMVG_lInftyComputerVision;openMVG_system;openMVG_matching"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/openMVG_sfm.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS openMVG_sfm )
list(APPEND _IMPORT_CHECK_FILES_FOR_openMVG_sfm "${_IMPORT_PREFIX}/lib/openMVG_sfm.lib" )

# Import target "vlsift" for configuration "Debug"
set_property(TARGET vlsift APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(vlsift PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "C"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/vlsift.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS vlsift )
list(APPEND _IMPORT_CHECK_FILES_FOR_vlsift "${_IMPORT_PREFIX}/lib/vlsift.lib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
