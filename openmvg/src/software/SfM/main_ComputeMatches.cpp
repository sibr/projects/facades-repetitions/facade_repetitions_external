
// Copyright (c) 2012, 2016 Pierre MOULON.

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "openMVG/sfm/sfm_data.hpp"
#include "openMVG/sfm/sfm_data_io.hpp"
#include "openMVG/sfm/pipelines/sfm_engine.hpp"
#include "openMVG/sfm/pipelines/sfm_features_provider.hpp"
#include "openMVG/sfm/pipelines/sfm_regions_provider.hpp"
#include "openMVG/sfm/pipelines/sfm_regions_provider_cache.hpp"

/// Generic Image Collection image matching
#include "openMVG/matching_image_collection/Matcher_Regions_AllInMemory.hpp"
#include "openMVG/matching_image_collection/Cascade_Hashing_Matcher_Regions_AllInMemory.hpp"
#include "openMVG/matching_image_collection/GeometricFilter.hpp"
#include "openMVG/matching_image_collection/F_ACRobust.hpp"
#include "openMVG/matching_image_collection/E_ACRobust.hpp"
#include "openMVG/matching_image_collection/H_ACRobust.hpp"
#include "openMVG/matching_image_collection/Pair_Builder.hpp"
#include "openMVG/matching/pairwiseAdjacencyDisplay.hpp"
#include "openMVG/matching/indMatch_utils.hpp"
#include "openMVG/system/timer.hpp"

#include "openMVG/graph/graph.hpp"
#include "openMVG/stl/stl.hpp"
#include "third_party/cmdLine/cmdLine.h"
#include "third_party/stlplus3/filesystemSimplified/file_system.hpp"

#include <cstdlib>
#include <fstream>

//#include "openMVG/matching_image_collection/Geometric_Filter_utils.hpp"

using namespace openMVG;
using namespace openMVG::cameras;
using namespace openMVG::matching;
using namespace openMVG::robust;
using namespace openMVG::sfm;
using namespace openMVG::matching_image_collection;
using namespace std;

enum EGeometricModel
{
  FUNDAMENTAL_MATRIX = 0,
  ESSENTIAL_MATRIX   = 1,
  HOMOGRAPHY_MATRIX  = 2
};

enum EPairMode
{
  PAIR_EXHAUSTIVE = 0,
  PAIR_CONTIGUOUS = 1,
  PAIR_FROM_FILE  = 2
};


struct BundleCamera {
	Eigen::Matrix4f viewProj;
	Eigen::Matrix3f rotation;
	Eigen::Vector3f position;
	float w;
	float h;
	float fovy;
};


Eigen::Vector3f getIntersection(const BundleCamera & cam, const Eigen::Vector2f & pixels, Eigen::Vector3f planeNormal, const Eigen::Vector3f & planePoint) {
	

	// Estimate world space position of the clicked pixel.
	Eigen::Vector3f dir1 = (cam.rotation * Eigen::Vector3f(0.0f, 0.0f, -1.0f)).normalized();
	Eigen::Vector3f up = (cam.rotation * Eigen::Vector3f(0.0f, 1.0f, 0.0f)).normalized();
	float aspect = cam.w / cam.h;

	Eigen::Vector2f screenWorldSize;
	float heightWorldSize = 2.f*tanf(cam.fovy / 2.f);
	screenWorldSize = Eigen::Vector2f(heightWorldSize*aspect, heightWorldSize);

	Eigen::Vector3f right = (dir1.cross(up)).normalized();
	Eigen::Vector3f rowSize = right*screenWorldSize[0];
	Eigen::Vector3f colSize = -up*screenWorldSize[1];

	Eigen::Vector3f dx = rowSize / cam.w;
	Eigen::Vector3f dy = colSize / cam.h;

	Eigen::Vector3f upLeftOffset = dir1 - rowSize / 2.f - colSize / 2.f;
	upLeftOffset += cam.position;
	Eigen::Vector3f worldPos = pixels(0)*dx + pixels(1)*dy + upLeftOffset;

	// Cast a ray.
	Eigen::Vector3f dir = (worldPos - cam.position).normalized();

	float orientation = dir.dot(planeNormal);
	// Check for parallelism
	//std::cout << orientation << std::endl;
	if (abs(orientation) < FLT_EPSILON) {
		//std::cout << "Ignoring a pair with ray parallel to the facade plane.\n";
		//std::cout << dir.transpose() << "          " << planeNormal.transpose() << std::endl;
		return Eigen::Vector3f(0.0f, 0.0f, 0.0f);
	}

	float abscisse = (planePoint - cam.position).dot(planeNormal) / orientation;
	return cam.position + abscisse * dir;
}

/// Compute corresponding features between a series of views:
/// - Load view images description (regions: features & descriptors)
/// - Compute putative local feature matches (descriptors matching)
/// - Compute geometric coherent feature matches (robust model estimation from putative matches)
/// - Export computed data
int main(int argc, char **argv)
{
  CmdLine cmd;

  std::string sSfM_Data_Filename;
  std::string sMatchesDirectory = "";
  std::string sBundlerFilename;
  std::string sGeometricModel = "f";
  float fDistRatio = 0.8f;
  int iMatchingVideoMode = -1;
  std::string sPredefinedPairList = "";
  std::string sNearestMatchingMethod = "AUTO";
  bool bForce = false;
  bool bGuided_matching = false;
  int imax_iteration = 2048;
  unsigned int ui_max_cache_size = 0;
  float fFilterRatio = 0.5f;
  //required
  cmd.add( make_option('i', sSfM_Data_Filename, "input_file") );
  cmd.add( make_option('o', sMatchesDirectory, "out_dir") );
  cmd.add(make_option('b', sBundlerFilename, "bundler_file"));
  // Options
  cmd.add( make_option('r', fDistRatio, "ratio") );
  cmd.add(make_option('d', fFilterRatio, "filter ratio"));
  cmd.add( make_option('g', sGeometricModel, "geometric_model") );
  cmd.add( make_option('v', iMatchingVideoMode, "video_mode_matching") );
  cmd.add( make_option('l', sPredefinedPairList, "pair_list") );
  cmd.add( make_option('n', sNearestMatchingMethod, "nearest_matching_method") );
  cmd.add( make_option('f', bForce, "force") );
  cmd.add( make_option('m', bGuided_matching, "guided_matching") );
  cmd.add( make_option('I', imax_iteration, "max_iteration") );
  cmd.add( make_option('c', ui_max_cache_size, "cache_size") );


  try {
      if (argc == 1) throw std::string("Invalid command line parameter.");
      cmd.process(argc, argv);
  } catch(const std::string& s) {
      std::cerr << "Usage: " << argv[0] << '\n'
      << "[-i|--input_file] a SfM_Data file\n"
	  << "[-b|--bundler_file] a bundler file\n"
      << "[-o|--out_dir path] output path where computed are stored\n"
      << "\n[Optional]\n"
      << "[-f|--force] Force to recompute data]\n"
      << "[-r|--ratio] Distance ratio to discard non meaningful matches\n"
      << "   0.8: (default).\n"
      << "[-g|--geometric_model]\n"
      << "  (pairwise correspondences filtering thanks to robust model estimation):\n"
      << "   f: (default) fundamental matrix,\n"
      << "   e: essential matrix,\n"
      << "   h: homography matrix.\n"
      << "[-v|--video_mode_matching]\n"
      << "  (sequence matching with an overlap of X images)\n"
      << "   X: with match 0 with (1->X), ...]\n"
      << "   2: will match 0 with (1,2), 1 with (2,3), ...\n"
      << "   3: will match 0 with (1,2,3), 1 with (2,3,4), ...\n"
      << "[-l]--pair_list] file\n"
      << "[-n|--nearest_matching_method]\n"
      << "  AUTO: auto choice from regions type,\n"
      << "  For Scalar based regions descriptor:\n"
      << "    BRUTEFORCEL2: L2 BruteForce matching,\n"
      << "    ANNL2: L2 Approximate Nearest Neighbor matching,\n"
      << "    CASCADEHASHINGL2: L2 Cascade Hashing matching.\n"
      << "    FASTCASCADEHASHINGL2: (default)\n"
      << "      L2 Cascade Hashing with precomputed hashed regions\n"
      << "     (faster than CASCADEHASHINGL2 but use more memory).\n"
      << "  For Binary based descriptor:\n"
      << "    BRUTEFORCEHAMMING: BruteForce Hamming matching.\n"
      << "[-m|--guided_matching]\n"
      << "  use the found model to improve the pairwise correspondences."
      << "[-c|--cache_size]\n"
      << "  Use a regions cache (only cache_size regions will be stored in memory)"
      << "  If not used, all regions will be load in memory."
      << std::endl;

      std::cerr << s << std::endl;
      return EXIT_FAILURE;
  }

  std::cout << " You called : " << "\n"
            << argv[0] << "\n"
            << "--input_file " << sSfM_Data_Filename << "\n"
			<< "--bundle_file " << sBundlerFilename << "\n"
            << "--out_dir " << sMatchesDirectory << "\n"
            << "Optional parameters:" << "\n"
            << "--force " << bForce << "\n"
            << "--ratio " << fDistRatio << "\n"
	  << "--filter_ratio " << fFilterRatio << "\n"
            << "--geometric_model " << sGeometricModel << "\n"
            << "--video_mode_matching " << iMatchingVideoMode << "\n"
            << "--pair_list " << sPredefinedPairList << "\n"
            << "--nearest_matching_method " << sNearestMatchingMethod << "\n"
            << "--guided_matching " << bGuided_matching << "\n"
            << "--cache_size " << ((ui_max_cache_size == 0) ? "unlimited" : std::to_string(ui_max_cache_size)) << std::endl;

  EPairMode ePairmode = (iMatchingVideoMode == -1 ) ? PAIR_EXHAUSTIVE : PAIR_CONTIGUOUS;

  if (sPredefinedPairList.length()) {
    ePairmode = PAIR_FROM_FILE;
    if (iMatchingVideoMode>0) {
      std::cerr << "\nIncompatible options: --videoModeMatching and --pairList" << std::endl;
      return EXIT_FAILURE;
    }
  }

  if (sMatchesDirectory.empty() || !stlplus::is_folder(sMatchesDirectory))  {
    std::cerr << "\nIt is an invalid output directory" << std::endl;
    return EXIT_FAILURE;
  }


  const std::string sFilteredMatchesFilename = "matches.filtered.bin";

  EGeometricModel eGeometricModelToCompute = FUNDAMENTAL_MATRIX;
  std::string sGeometricMatchesFilename = "";
  switch(sGeometricModel[0])
  {
    case 'f': case 'F':
      eGeometricModelToCompute = FUNDAMENTAL_MATRIX;
      sGeometricMatchesFilename = "matches.f.bin";
    break;
    case 'e': case 'E':
      eGeometricModelToCompute = ESSENTIAL_MATRIX;
      sGeometricMatchesFilename = "matches.e.bin";
    break;
    case 'h': case 'H':
      eGeometricModelToCompute = HOMOGRAPHY_MATRIX;
      sGeometricMatchesFilename = "matches.h.bin";
    break;
    default:
      std::cerr << "Unknown geometric model" << std::endl;
      return EXIT_FAILURE;
  }

  // -----------------------------
  // - Load SfM_Data Views & intrinsics data
  // a. Compute putative descriptor matches
  // b. Geometric filtering of putative matches
  // + Export some statistics
  // -----------------------------

  //---------------------------------------
  // Read SfM Scene (image view & intrinsics data)
  //---------------------------------------
  SfM_Data sfm_data;
  if (!Load(sfm_data, sSfM_Data_Filename, ESfM_Data(VIEWS|INTRINSICS))) {
    std::cerr << std::endl
      << "The input SfM_Data file \""<< sSfM_Data_Filename << "\" cannot be read." << std::endl;
    return EXIT_FAILURE;
  }

  //---------------------------------------
  // Load SfM Scene regions
  //---------------------------------------
  // Init the regions_type from the image describer file (used for image regions extraction)
  using namespace openMVG::features;
  const std::string sImage_describer = stlplus::create_filespec(sMatchesDirectory, "image_describer", "json");
  std::unique_ptr<Regions> regions_type = Init_region_type_from_file(sImage_describer);
  if (!regions_type)
  {
    std::cerr << "Invalid: "
      << sImage_describer << " regions type file." << std::endl;
    return EXIT_FAILURE;
  }

  //---------------------------------------
  // a. Compute putative descriptor matches
  //    - Descriptor matching (according user method choice)
  //    - Keep correspondences only if NearestNeighbor ratio is ok
  //---------------------------------------

  // Load the corresponding view regions
  std::shared_ptr<Regions_Provider> regions_provider;
  if (ui_max_cache_size == 0)
  {
    // Default regions provider (load & store all regions in memory)
    regions_provider = std::make_shared<Regions_Provider>();
  }
  else
  {
    // Cached regions provider (load & store regions on demand)
    regions_provider = std::make_shared<Regions_Provider_Cache>(ui_max_cache_size);
  }

  if (!regions_provider->load(sfm_data, sMatchesDirectory, regions_type)) {
    std::cerr << std::endl << "Invalid regions." << std::endl;
    return EXIT_FAILURE;
  }

  PairWiseMatches map_PutativesMatches;

  // Build some alias from SfM_Data Views data:
  // - List views as a vector of filenames & image sizes
  std::vector<std::string> vec_fileNames;
  std::vector<std::pair<size_t, size_t> > vec_imagesSize;
  {
    vec_fileNames.reserve(sfm_data.GetViews().size());
    vec_imagesSize.reserve(sfm_data.GetViews().size());
    for (Views::const_iterator iter = sfm_data.GetViews().begin();
      iter != sfm_data.GetViews().end();
      ++iter)
    {
      const View * v = iter->second.get();
      vec_fileNames.push_back(stlplus::create_filespec(sfm_data.s_root_path,
          v->s_Img_path));
      vec_imagesSize.push_back( std::make_pair( v->ui_width, v->ui_height) );
    }
  }

  std::cout << std::endl << " - PUTATIVE MATCHES - " << std::endl;
  // If the matches already exists, reload them
  if
  (
    !bForce
    && (stlplus::file_exists(sMatchesDirectory + "/matches.putative.txt")
        || stlplus::file_exists(sMatchesDirectory + "/matches.putative.bin"))
  )
  {
    if (!(Load(map_PutativesMatches, sMatchesDirectory + "/matches.putative.bin") ||
          Load(map_PutativesMatches, sMatchesDirectory + "/matches.putative.txt")) )
    {
      std::cerr << "Cannot load input matches file";
      return EXIT_FAILURE;
    }
    std::cout << "\t PREVIOUS RESULTS LOADED;"
      << " #pair: " << map_PutativesMatches.size() << std::endl;
  }
  else // Compute the putative matches
  {
    std::cout << "Use: ";
    switch (ePairmode)
    {
      case PAIR_EXHAUSTIVE: std::cout << "exhaustive pairwise matching" << std::endl; break;
      case PAIR_CONTIGUOUS: std::cout << "sequence pairwise matching" << std::endl; break;
      case PAIR_FROM_FILE:  std::cout << "user defined pairwise matching" << std::endl; break;
    }

    // Allocate the right Matcher according the Matching requested method
    std::unique_ptr<Matcher> collectionMatcher;
    if (sNearestMatchingMethod == "AUTO")
    {
      if (regions_type->IsScalar())
      {
        std::cout << "Using FAST_CASCADE_HASHING_L2 matcher" << std::endl;
        collectionMatcher.reset(new Cascade_Hashing_Matcher_Regions_AllInMemory(fDistRatio));
      }
      else
      if (regions_type->IsBinary())
      {
        std::cout << "Using BRUTE_FORCE_HAMMING matcher" << std::endl;
        collectionMatcher.reset(new Matcher_Regions_AllInMemory(fDistRatio, BRUTE_FORCE_HAMMING));
      }
    }
    else
    if (sNearestMatchingMethod == "BRUTEFORCEL2")
    {
      std::cout << "Using BRUTE_FORCE_L2 matcher" << std::endl;
      collectionMatcher.reset(new Matcher_Regions_AllInMemory(fDistRatio, BRUTE_FORCE_L2));
    }
    else
    if (sNearestMatchingMethod == "BRUTEFORCEHAMMING")
    {
      std::cout << "Using BRUTE_FORCE_HAMMING matcher" << std::endl;
      collectionMatcher.reset(new Matcher_Regions_AllInMemory(fDistRatio, BRUTE_FORCE_HAMMING));
    }
    else
    if (sNearestMatchingMethod == "ANNL2")
    {
      std::cout << "Using ANN_L2 matcher" << std::endl;
      collectionMatcher.reset(new Matcher_Regions_AllInMemory(fDistRatio, ANN_L2));
    }
    else
    if (sNearestMatchingMethod == "CASCADEHASHINGL2")
    {
      std::cout << "Using CASCADE_HASHING_L2 matcher" << std::endl;
      collectionMatcher.reset(new Matcher_Regions_AllInMemory(fDistRatio, CASCADE_HASHING_L2));
    }
    else
    if (sNearestMatchingMethod == "FASTCASCADEHASHINGL2")
    {
      std::cout << "Using FAST_CASCADE_HASHING_L2 matcher" << std::endl;
      collectionMatcher.reset(new Cascade_Hashing_Matcher_Regions_AllInMemory(fDistRatio));
    }
    if (!collectionMatcher)
    {
      std::cerr << "Invalid Nearest Neighbor method: " << sNearestMatchingMethod << std::endl;
      return EXIT_FAILURE;
    }
    // Perform the matching
    system::Timer timer;
    {
      // From matching mode compute the pair list that have to be matched:
      Pair_Set pairs;
      switch (ePairmode)
      {
        case PAIR_EXHAUSTIVE: pairs = exhaustivePairs(sfm_data.GetViews().size()); break;
        case PAIR_CONTIGUOUS: pairs = contiguousWithOverlap(sfm_data.GetViews().size(), iMatchingVideoMode); break;
        case PAIR_FROM_FILE:
          if(!loadPairs(sfm_data.GetViews().size(), sPredefinedPairList, pairs))
          {
              return EXIT_FAILURE;
          }
          break;
      }
      // Photometric matching of putative pairs
      collectionMatcher->Match(sfm_data, regions_provider, pairs, map_PutativesMatches);
      //---------------------------------------
      //-- Export putative matches
      //---------------------------------------
      if (!Save(map_PutativesMatches, std::string(sMatchesDirectory + "/matches.putative.bin")))
      {
        std::cerr
          << "Cannot save computed matches in: "
          << std::string(sMatchesDirectory + "/matches.putative.bin");
        return EXIT_FAILURE;
      }
    }
    std::cout << "Task (Regions Matching) done in (s): " << timer.elapsed() << std::endl;
  }
  //-- export putative matches Adjacency matrix
  PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(),
    map_PutativesMatches,
    stlplus::create_filespec(sMatchesDirectory, "PutativeAdjacencyMatrix", "svg"));
  //-- export view pair graph once putative graph matches have been computed
  {
    std::set<IndexT> set_ViewIds;
    std::transform(sfm_data.GetViews().begin(), sfm_data.GetViews().end(),
      std::inserter(set_ViewIds, set_ViewIds.begin()), stl::RetrieveKey());
    graph::indexedGraph putativeGraph(set_ViewIds, getPairs(map_PutativesMatches));
    graph::exportToGraphvizData(
      stlplus::create_filespec(sMatchesDirectory, "putative_matches"),
      putativeGraph);
  }

  // here load file with matrices, perform reprojection for each putative match, and reject those which don't fall closes when reprojected using approx cameras.
  //---------------------------------------
  // b. Geometric filtering of putative matches
  //    - AContrario Estimation of the desired geometric model
  //    - Use an upper bound for the a contrario estimated threshold
  //---------------------------------------


  
  // Load bundler
  
  std::ifstream bundle_file(sBundlerFilename);
  if (bundle_file.is_open()) {
	  
	  std::vector<BundleCamera> _bundleCameras;

	  // read number of images 
	  std::string line;
	  getline(bundle_file, line);	// ignore first line - contains version
	  // next three lines contain the 3 plane points.
	  float points[9];
	  bundle_file >> points[0] >> points[1] >> points[2];
	  bundle_file >> points[3] >> points[4] >> points[5];
	  bundle_file >> points[6] >> points[7] >> points[8];

	  Eigen::Vector3f p0(points[0], points[1], points[2]);
	  Eigen::Vector3f p1(points[3], points[4], points[5]);
	  Eigen::Vector3f p2(points[6], points[7], points[8]);

	  std::cout << "Plane: " << p0.transpose() << "    " << p1.transpose() << "    " << p2.transpose() << std::endl;
	  Eigen::Vector3f planeNormal = ((p1 - p0).normalized().cross((p2 - p0).normalized()).normalized()).normalized();

	  int numImages = 0;
	  bundle_file >> numImages;	// read number of images)
	  getline(bundle_file, line);	// ignore the rest of the line

	  // Strong hypothesis : cameras are ordered in the same order as crop images.
	  for (int i = 0; i<numImages ; i++) {
		  float m[15]; // bundler params
		  bundle_file >> m[0] >> m[1] >> m[2] >> m[3] >> m[4];
		  bundle_file >> m[5] >> m[6] >> m[7] >> m[8] >> m[9];
		  bundle_file >> m[10] >> m[11] >> m[12] >> m[13] >> m[14];
		  
		  Eigen::Matrix3f matRotation;
		  matRotation <<  m[3], m[4], m[5],
						  m[6], m[7], m[8],
						  m[9], m[10], m[11];
		  matRotation = matRotation.transpose().eval();
		  
		  Eigen::Vector3f position = Eigen::Vector3f(
			  -(m[3] * m[12] + m[6] * m[13] + m[9] * m[14]),
			  -(m[4] * m[12] + m[7] * m[13] + m[10] * m[14]),
			  -(m[5] * m[12] + m[8] * m[13] + m[11] * m[14]));// (-matRotation * Eigen::Vector3f(m[12], m[13], m[14])).eval();
		 

		  float tempw = float(sfm_data.views.at(i)->ui_width);
		  float temph = float(sfm_data.views.at(i)->ui_height);


		  Eigen::Matrix4f translationMat;
		  translationMat.setIdentity();
		  translationMat(0, 3) = position[0];
		  translationMat(1, 3) = position[1];
		  translationMat(2, 3) = position[2];

		  Eigen::Matrix4f rotation4Mat;
		  rotation4Mat <<	matRotation(0, 0), matRotation(0, 1), matRotation(0, 2), 0.0f,
							matRotation(1, 0), matRotation(1, 1), matRotation(1, 2), 0.0f,
							matRotation(2, 0), matRotation(2, 1), matRotation(2, 2), 0.0f,
							0.0f,				0.0f,				0.0f,			 1.0f;

		  Eigen::Matrix4f view = (translationMat * rotation4Mat).inverse().eval();

		  Eigen::Matrix4f proj;
		  float yScale = float(1.0) / (0.5f*temph / m[0]) ; // 1/tan(0.5 * fov) -> ok
		  float xScale = yScale / (tempw / temph);
		  proj << xScale, 0, 0, 0,
			  0, yScale, 0, 0,
			  0, 0, 1000.01 / (0.01 - 1000.0), 2 * 0.01*1000.0 / (0.01 - 1000.0),
			  0, 0, -1.0, 0;

		  BundleCamera cam;
		  cam.viewProj = (proj*view).eval();
		  cam.rotation = matRotation;
		  cam.position = position;
		  cam.w = tempw;
		  cam.h = temph;
		  cam.fovy = 2.0f * atan(0.5f*temph / m[0]);

		  _bundleCameras.push_back(cam);
	  }

	  bundle_file.close();
#define DEBUG_EXPORT_PLY
#ifdef DEBUG_EXPORT_PLY			
	  std::ofstream file3;
	  file3.open(sMatchesDirectory +"/points.xyz", std::ios::out);
	  if (!file3.is_open()) {
		 std::cout << "Unable to write plane points to file" << std::endl;
		  return -1;
	  }
	//  file3 << p0[0] << " " << p0[1] << " " << p0[2] << std::endl;
	  file3 << p1[0] - p0[0] << " " << p1[1] - p0[1] << " " << p1[2] - p0[2] << std::endl;
	  file3 << p2[0] - p0[0] << " " << p2[1] - p0[1] << " " << p2[2] - p0[2] << std::endl;
	  for (auto& cam : _bundleCameras) {
		 // std::cout << cam.position[0] << " " << cam.position[1] << " " << cam.position[2]  << std::endl;
		  file3 << cam.position[0] << " " << cam.position[1] << " " << cam.position[2] << std::endl;
	  }
#endif

	  PairWiseMatches map_FilteredMatches;

	  for (PairWiseMatches::const_iterator iterMap = map_PutativesMatches.begin();
		  iterMap != map_PutativesMatches.end(); ++iterMap)
	  {
		  size_t iIndex = iterMap->first.first;
		  size_t jIndex = iterMap->first.second;
		  std::cout << "Pair " << iIndex << "," << jIndex << " : ";

		  auto camI = _bundleCameras[iIndex];
		  auto camJ = _bundleCameras[jIndex];

		  
		  IndMatches filtered_inliers;
		  filtered_inliers.clear();

		  Mat xI, xJ;
		  matching_image_collection::MatchesPairToMat(iterMap->first, iterMap->second, &sfm_data, regions_provider, xI, xJ);

	
		  double sum = 0.0;
		  for (size_t pairId = 0; pairId < xI.cols(); ++pairId) {
			  // Project each features point onto the plane using camera matrix loaded from bundler file
			  Eigen::Vector2f screenI(xI(0, pairId), xI(1, pairId));
			  Eigen::Vector2f screenJ(xJ(0, pairId), xJ(1, pairId));
			  Eigen::Vector3f worldI = getIntersection(camI, screenI, planeNormal, Eigen::Vector3f(0.0f, 0.0f, 0.0f));
			  Eigen::Vector3f worldJ = getIntersection(camJ, screenJ, planeNormal, Eigen::Vector3f(0.0f, 0.0f, 0.0f));

#ifdef DEBUG_EXPORT_PLY	
			  file3 << worldI[0] << " " << worldI[1] << " " << worldI[2]  << std::endl;
			  file3 << worldJ[0] << " " << worldJ[1] << " " << worldJ[2]  << std::endl;
#endif

			  // Check distance (reprojected ?)
			  // Reproject pointI into camJ frame
			  Eigen::Vector4f reprojectedI = camJ.viewProj * Eigen::Vector4f(worldI[0], worldI[1], worldI[2], 1.0);
			  reprojectedI = reprojectedI / reprojectedI[3];
			  Eigen::Vector2f screenReprojectedI(camJ.w*0.5f*(reprojectedI[0] + 1.0f), camJ.h*0.5f*(1.0f - reprojectedI[1]));

			  Eigen::Vector4f reprojectedJ = camI.viewProj * Eigen::Vector4f(worldJ[0], worldJ[1], worldJ[2], 1.0);
			  reprojectedJ = reprojectedJ / reprojectedJ[3];
			  Eigen::Vector2f screenReprojectedJ(camI.w*0.5f*(reprojectedJ[0] + 1.0f), camI.h*0.5f*(1.0f - reprojectedJ[1]));

			  /*
			  
			  float diagonalI = std::sqrt(camI.w*camI.w + camI.h * camI.h);
			  float diagonalJ = std::sqrt(camJ.w*camJ.w + camJ.h * camJ.h);

			  float normalizationI = sqrt(2.0f) / diagonalI;
			  float normalizationJ = sqrt(2.0f) / diagonalJ;
			  float distanceI = (screenReprojectedI - screenJ).norm() * normalizationJ;
			  float distanceJ = (screenReprojectedJ - screenI).norm() * normalizationI;
			  */
			//  std::cout << "(" << distanceI << "," << distanceJ << ")" << std::endl;
			  
			  float distanceI = (screenReprojectedI - screenJ).cwiseQuotient(Eigen::Vector2f(camJ.w, camJ.h)).norm();
			  float distanceJ = (screenReprojectedJ - screenI).cwiseQuotient(Eigen::Vector2f(camI.w, camI.h)).norm();
			  
			  sum += distanceI + distanceJ;
			  if (distanceI < fFilterRatio && distanceJ < fFilterRatio) {
				  // the pair will be kept
				  filtered_inliers.push_back(iterMap->second[pairId]);
			  }
			  
			
		  }

		  sum = sum / (xI.cols()*2.0);
		  std::cout << "Count: " << filtered_inliers.size() << "/" << iterMap->second.size()  << " (" << 100.0*float(filtered_inliers.size())/float(iterMap->second.size()) << "%) [avg=" << sum << "]" << std::endl;

		  map_FilteredMatches.insert(std::make_pair(iterMap->first, std::move(filtered_inliers)));
	  }

#ifdef DEBUG_EXPORT_PLY	
	  file3.close();
#endif
	  
	  /*

	  if (!Save(map_FilteredMatches,
		  std::string(sMatchesDirectory + "/" + sFilteredMatchesFilename)))
	  {
		  std::cerr
			  << "Cannot save computed matches in: "
			  << std::string(sMatchesDirectory + "/" + sFilteredMatchesFilename);
		  return EXIT_FAILURE;
	  }


	  //-- export Adjacency matrix
	  std::cout << "\n Export Adjacency Matrix of the pairwise's geometric matches"
		  << std::endl;
	  PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(),
		  map_FilteredMatches,
		  stlplus::create_filespec(sMatchesDirectory, "FilteredAdjacencyMatrix", "svg"));

	  //-- export view pair graph once geometric filter have been done
	  {
		  std::set<IndexT> set_ViewIds;
		  std::transform(sfm_data.GetViews().begin(), sfm_data.GetViews().end(),
			  std::inserter(set_ViewIds, set_ViewIds.begin()), stl::RetrieveKey());
		  graph::indexedGraph putativeGraph(set_ViewIds, getPairs(map_FilteredMatches));
		  graph::exportToGraphvizData(
			  stlplus::create_filespec(sMatchesDirectory, "filtered_matches"),
			  putativeGraph);
	  }
	  */
	  std::swap(map_PutativesMatches, map_FilteredMatches);

  } else {
	std::cout << "---------------------\n----- Skipping frustum filtering. -----\n--------------------------" << std::endl;
  }

  
  std::unique_ptr<ImageCollectionGeometricFilter> filter_ptr(
    new ImageCollectionGeometricFilter(&sfm_data, regions_provider));

  if (filter_ptr)
  {
    system::Timer timer;
    std::cout << std::endl << " - Geometric filtering - " << std::endl;

    PairWiseMatches map_GeometricMatches;
    switch (eGeometricModelToCompute)
    {
      case HOMOGRAPHY_MATRIX:
      {
        const bool bGeometric_only_guided_matching = true;
        filter_ptr->Robust_model_estimation(GeometricFilter_HMatrix_AC(4.0, imax_iteration),
          map_PutativesMatches, bGuided_matching,
          bGeometric_only_guided_matching ? -1.0 : 0.6);
        map_GeometricMatches = filter_ptr->Get_geometric_matches();
      }
      break;
      case FUNDAMENTAL_MATRIX:
      {
        filter_ptr->Robust_model_estimation(GeometricFilter_FMatrix_AC(4.0, imax_iteration),
          map_PutativesMatches, bGuided_matching);
        map_GeometricMatches = filter_ptr->Get_geometric_matches();
      }
      break;
      case ESSENTIAL_MATRIX:
      {
        filter_ptr->Robust_model_estimation(GeometricFilter_EMatrix_AC(4.0, imax_iteration),
          map_PutativesMatches, bGuided_matching);
        map_GeometricMatches = filter_ptr->Get_geometric_matches();

        //-- Perform an additional check to remove pairs with poor overlap
        std::vector<PairWiseMatches::key_type> vec_toRemove;
        for (PairWiseMatches::const_iterator iterMap = map_GeometricMatches.begin();
          iterMap != map_GeometricMatches.end(); ++iterMap)
        {
          const size_t putativePhotometricCount = map_PutativesMatches.find(iterMap->first)->second.size();
          const size_t putativeGeometricCount = iterMap->second.size();
          const float ratio = putativeGeometricCount / (float)putativePhotometricCount;
          if (putativeGeometricCount < 50 || ratio < .3f)  {
            // the pair will be removed
            vec_toRemove.push_back(iterMap->first);
          }
        }
        //-- remove discarded pairs
        for (std::vector<PairWiseMatches::key_type>::const_iterator
          iter =  vec_toRemove.begin(); iter != vec_toRemove.end(); ++iter)
        {
          map_GeometricMatches.erase(*iter);
        }
      }
      break;
    }

    //---------------------------------------
    //-- Export geometric filtered matches
    //---------------------------------------
    if (!Save(map_GeometricMatches,
      std::string(sMatchesDirectory + "/" + sGeometricMatchesFilename)))
    {
      std::cerr
          << "Cannot save computed matches in: "
          << std::string(sMatchesDirectory + "/" + sGeometricMatchesFilename);
      return EXIT_FAILURE;
    }

    std::cout << "Task done in (s): " << timer.elapsed() << std::endl;

    //-- export Adjacency matrix
    std::cout << "\n Export Adjacency Matrix of the pairwise's geometric matches"
      << std::endl;
    PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(),
      map_GeometricMatches,
      stlplus::create_filespec(sMatchesDirectory, "GeometricAdjacencyMatrix", "svg"));

    //-- export view pair graph once geometric filter have been done
    {
      std::set<IndexT> set_ViewIds;
      std::transform(sfm_data.GetViews().begin(), sfm_data.GetViews().end(),
        std::inserter(set_ViewIds, set_ViewIds.begin()), stl::RetrieveKey());
      graph::indexedGraph putativeGraph(set_ViewIds, getPairs(map_GeometricMatches));
      graph::exportToGraphvizData(
        stlplus::create_filespec(sMatchesDirectory, "geometric_matches"),
        putativeGraph);
    }
  }
  return EXIT_SUCCESS;
}
